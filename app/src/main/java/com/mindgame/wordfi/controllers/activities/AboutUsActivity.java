package com.mindgame.wordfi.controllers.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.mindgame.wordfi.R;
import com.mindgame.wordfi.services.Screen;
/***
 * @author Nils
 *
 *
 */
public class AboutUsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Enable Full Screen
        new Screen(this).fullMode();
        setContentView(R.layout.activity_about_us);

        MaterialButton back = findViewById(R.id.aboutUs_back_button);
        TextView messageTextView = findViewById(R.id.about_us_message);

        messageTextView.setMovementMethod(new ScrollingMovementMethod());

        back.setOnClickListener(v -> {
            Intent intent=new Intent(this, SettingActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        });
    }
}