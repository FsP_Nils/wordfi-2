package com.mindgame.wordfi.controllers.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.mindgame.wordfi.R;
import com.mindgame.wordfi.services.Settings;
import com.mindgame.wordfi.services.CoreData;
import com.mindgame.wordfi.services.Screen;
import com.mindgame.wordfi.services.SoundManager;

import org.jetbrains.annotations.NotNull;

/***
 * @author Nils
 */
public class DashboardActivity extends AppCompatActivity {

    private ImageButton image_sound_button;
    private TextView level_textView;
    private CoreData coreData;
    private ImageView level_state_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Enable Full Screen
        new Screen(this).fullMode();
        setContentView(R.layout.activity_dashboard);

        coreData = new CoreData(this);
        Settings.setLanguageTo(this,coreData.getLanguage());
        String levelMode = Settings.getLevel(this);

        level_textView = findViewById(R.id.dashboard_level_state);
        level_state_image = findViewById(R.id.level_image);
        image_sound_button = findViewById(R.id.dashboard_sound_button);
        MaterialButton playActionButton =  findViewById(R.id.dashboard_play_button);
        ImageButton image_setting_button = findViewById(R.id.dashboard_setting_button);
        MaterialButton customActionButton = findViewById(R.id.custom_round_action_button);
        level_textView.setText(levelMode);
        getLevel_state_image(levelMode);
        if(coreData.getSoundState()){
            image_sound_button.setImageResource(R.drawable.sound_on_icon);
        }else {
            image_sound_button.setImageResource(R.drawable.sound_mute_icon);
        }

        playActionButton.setOnClickListener(view -> startGame());
        customActionButton.setOnClickListener(view -> goToCustomRoundActivity());
        image_setting_button.setOnClickListener(view -> goToSettingActivity());
        image_sound_button.setOnClickListener(view -> {
            if(SoundManager.isPlaying()){
                coreData.saveSoundState(!coreData.getSoundState());
                changeSoundState(coreData.getSoundState());
            }else {
                SoundManager.startBackgroundMusic();
                coreData.saveSoundState(true);
            }
        });
    }

    /**
     * Start Local Game Activity
      */
    private void startGame(){
         Intent intent = new Intent(this, PlayActivity.class);
         startActivity(intent);
         finish();
     }

    /**
     * Start Local Setting Activity
     */
     private void goToSettingActivity(){
         Intent intent = new Intent(this, SettingActivity.class);
         startActivity(intent);
         finish();
     }

    /**
     * Start Local Custom Game Activity
     */
    private void goToCustomRoundActivity() {
        Intent intent = new Intent(this, CustomRoundActivity.class);
        startActivity(intent);
        finish();
    }


    /**
     * Change Sound State (Mute / On)
     */
    private void changeSoundState(boolean value){
        if(value){
            image_sound_button.setImageResource(R.drawable.sound_on_icon);
            SoundManager.setVolumeTo(coreData.getVolume());
        }else {
            image_sound_button.setImageResource(R.drawable.sound_mute_icon);
            SoundManager.mute();
        }
     }

    /**
     * provide an Image for every level_Mode
     */
     private void getLevel_state_image(@NotNull String state){
         String[] levels = this.getResources().getStringArray(R.array.level);
         if(state.equals(levels[1])){
             level_state_image.setImageResource(R.drawable.intermediate_level);
         }else if(state.equals(levels[2])){
             level_state_image.setImageResource(R.drawable.advanced_level);
         }else if(state.equals(levels[3])){
             level_state_image.setImageResource(R.drawable.expert_level);
         }else{
             level_state_image.setImageResource(R.drawable.beginner_level);
         }
     }
}