package com.mindgame.wordfi.services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;


public class CoreData {
    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor editor;
    private final String onBoardingStateKey;
    private final String highScoreKey;
    private final String themeKey;
    private final String languageKey;
    private final String levelKey;
    private final String soundKey;
    private final String volumeKey;
    private final String bestTime;

    /***
     * @author Rodrigue Ngalani Touko
     *
     *
     */
    @SuppressLint("CommitPrefEdits")
    public CoreData(Context context) {
        this.sharedPreferences = context.getSharedPreferences("App_Preferences",Context.MODE_PRIVATE);
        this.editor = sharedPreferences.edit();

        onBoardingStateKey = "onBoardingState";
        highScoreKey = "highScore";
        themeKey = "theme";
        languageKey = "language";
        levelKey = "level";
        soundKey = "sound";
        volumeKey = "volume";
        bestTime = "bestTime";
    }

    /**
     *
     * @param value
     */
    public void saveOnBoardingState(Boolean value){
        editor.putBoolean(onBoardingStateKey,value);
        editor.commit();
    }

    /**
     *
     * @return
     */
    public Boolean getOnBoardingState(){
        return sharedPreferences.getBoolean(onBoardingStateKey,false);
    }

    /**
     *
     * @param value
     */
    public void saveHighScore(long value){
        editor.putLong(highScoreKey,value);
        editor.commit();
    }

    /**
     *
     * @return
     */
    public long getHighScore(){
        return sharedPreferences.getLong(highScoreKey,00);
    }

    /**
     *
     * @param value
     */
    public void saveTheme(int value){
        editor.putInt(themeKey,value);
        editor.commit();
    }

    /**
     *
     * @return
     */
    public int getTheme(){
        return sharedPreferences.getInt(themeKey,0);
    }

    /**
     *
     * @param value
     */
    public void saveLanguage(int value){
        editor.putInt(languageKey,value);
        editor.commit();
    }
    public int getLanguage(){
        return sharedPreferences.getInt(languageKey,3);
    }

    /**
     *
     * @param value
     */
    public void saveLevel(int value){
        editor.putInt(levelKey,value);
        editor.commit();
    }

    /**
     *
     * @return
     */
    public int getLevel(){
        return sharedPreferences.getInt(levelKey,0);
    }

    /**
     *
     * @param state
     */
    public void saveSoundState(Boolean state){
        editor.putBoolean(soundKey,state);
        editor.commit();
    }

    /**
     *
     * @return
     */
    public Boolean getSoundState(){
        return  sharedPreferences.getBoolean(soundKey,true);
    }

    /**
     *
     * @param value
     */
    public void saveVolume(int value){
        editor.putInt(volumeKey,value);
        editor.commit();
    }

    /**
     *
     * @return
     */
    public int getVolume(){
        return sharedPreferences.getInt(volumeKey,50);
    }

    /**
     *
     * @param bestTime
     */
    public void saveBestTime(String bestTime){
        editor.putString(volumeKey,bestTime);
        editor.commit();
    }

    /**
     *
     * @return
     */
    public String getBestTime(){
        return sharedPreferences.getString(bestTime,"00:00:00");
    }

}
