package com.mindgame.wordfi.services;

import android.app.Activity;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;

/**
     * @author Rodrigue Touko
     *
     *Remove Navigation and Title Bar Form Screen
     * Set FullScreen on the Selected Activity
     */


    public class Screen {

        private final Activity activity;

        public Screen(Activity activity) {
            this.activity = activity;
        }

        public void fullMode() {
            int flags = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_FULLSCREEN;
                flags |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            activity.getWindow().getDecorView().setSystemUiVisibility(flags);
        }

        public float getWith(){
            Resources resources = activity.getResources();
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            return displayMetrics.widthPixels;
        }

    }

