package com.mindgame.wordfi.model;

import com.mindgame.wordfi.resources.Level;
import com.mindgame.wordfi.resources.RoundMode;

/***
 * @author Rodrigue Ngalani Touko
 *
 */
public class Round {
    // for a Round
    final RoundMode mode;
    final Level level;
    final Word[] words;
    final char[] characters;
    private int score;


    public Round(Level level, Word[] words, char[] characters,  RoundMode mode) {
        this.level = level;
        this.words = words;
        this.characters = characters;
        this.mode = mode;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

}
