package com.mindgame.wordfi.widgets;


import android.app.Activity;
import android.content.res.ColorStateList;
import android.widget.TextView;

import com.mindgame.wordfi.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/***
 * @author Rodrigue Ngalani Touko
 *
 *
 */
public class Solver {

    private final int puzzleLength;
    private final TextView[][] grid;
    private final Activity activity;
    private int score = 0;
    private final TextView scoreState;
    private final List<String> wordsList;

    public Solver(Activity activity, Puzzle puzzle, List<String> wordsList) {
        this.activity = activity;
        this.puzzleLength = puzzle.getBoardLength();
        this.grid = puzzle.getGrid();
        this.wordsList = wordsList;
        scoreState = activity.findViewById(R.id.score_state);
        setScore(score);
    }

    // For searching left to right and top to bottom
     int[] x = { 0,1 };
     int[] y = { 1,0 };

    /**
     *
     * @param grid
     * @param row
     * @param col
     * @param word
     * @return
     */
     boolean isInGrid(TextView[] @NotNull [] grid, int row, int col, @NotNull String word)
    {
        TextView[][] foundWord = new TextView[puzzleLength][puzzleLength];

        if (grid[row][col].getText().charAt(0) != word.charAt(0)) {
            return false;
        }
        else if(grid[row][col].getText().charAt(0) == word.charAt(0))
        {
            foundWord[row][col] = grid[row][col];

            for (int direction = 0; direction < 2; direction++) {

                int length;
                int rowDirection = row + x[direction];
                int columnDirection = col + y[direction];


                for (length = 1; length < word.length(); length++) {

                    if (rowDirection >= this.puzzleLength || rowDirection < 0 || columnDirection >= this.puzzleLength || columnDirection < 0)
                        break;

                    if (grid[rowDirection][columnDirection].getText().charAt(0) != word.charAt(length))
                        break;

                    foundWord[rowDirection][columnDirection] = grid[rowDirection][columnDirection];

                    // Moving in particular direction
                    rowDirection += x[direction];
                    columnDirection += y[direction];

                }

                if (length == word.length() && word.length() >= 3 && grid[row - 1][col].getText().charAt(0) == ' '  && grid[rowDirection][columnDirection].getText().charAt(0) == ' ' ) {
                    for (int i = 0; i < foundWord.length; i++) {
                        for (int j = 0; j < foundWord.length; j++) {
                            if( foundWord[i][j] != null  ) {
                                grid[i][j].setTextColor(activity.getColor(R.color.error_color));
                            }
                        }
                    }
                    setScore(word.length());
                    return true;
                    // check if the word is Vertical in Grid
                }else if(length == word.length() && word.length() >= 3 && grid[row][col - 1].getText().charAt(0) == ' ' && grid[rowDirection][columnDirection - 1].getText().charAt(0) != ' ' && grid[rowDirection][columnDirection].getText().charAt(0) == ' '){
                    for (int i = 0; i < foundWord.length; i++) {
                        for (int j = 0; j < foundWord.length; j++) {
                            if( foundWord[i][j] != null) {
                                grid[i][j].setTextColor(activity.getColor(R.color.error_color));
                            }
                        }
                    }
                    setScore(word.length());
                    return true;
                }
            }
        }
        return false;
    }


    boolean search(String word){
        for(int i = 0; i < word.length(); i++) {
            for (int row = 0; row < grid.length; row++) {
                for (int col = 0; col < grid.length; col++) {
                    if (word.charAt(i) == grid[row][col].getText().charAt(0)) {
                        // checking Horizontal
                        if(grid[][]){
                       //BUG
                        }
                    }
                }
            }
        }
    }
    public void searchHorizontally(){

    }

    public void searchVertically{

    }


    /**
     *
     * @param word
     */
     public void check(@NotNull String word)
    {
        if(!word.isEmpty()){
            for (int row = 0; row < puzzleLength; row++) {
                for (int col = 0; col < puzzleLength; col++) {
                    if (isInGrid(grid, row, col, word)) {
                        //TODO: Perform a sound effet
                        //TODO: increase the Score
                    }
                }
            }
        }

    }

    private void setScore(int value){
         score = score + value;
        final String format = String.format("%s / %s ",
                String.format("%02d", score),
                String.format("%02d", wordsList.size()));
        scoreState.setText(format);
     }

    /**
     *
     */
    public void help(){
         boolean helped = false;
         ColorStateList filledCaseColor = activity.getColorStateList(R.color.error_color);
         for (int i = 0; i < grid.length ; i++){
             for(int j = 0; j< grid.length; j++){
                 if(grid[i][j].getText() != " " && grid[i][j].getTextColors() != filledCaseColor && !helped){
                     grid[i][j].setTextColor(activity.getColor(R.color.error_color));
                     //helped = true;
                 }
             }
         }
    }

}
